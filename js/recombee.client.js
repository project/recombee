/**
 * @file
 * Initializes Recombee API Client.
 */

(function (settings) {

  'use strict';

  if (typeof settings.recombeeClient === 'undefined') {
    console.debug('RecombeeClient: Config not available.');
    return;
  }
  var config = settings.recombeeClient;
  var client = getClient(config.account, config.token, config.options);

  if (typeof client !== 'undefined') {
    window.RecombeeClient = client;
  }
  else {
    console.error('Recombee: Client not available.');
  }

  function getClient(databaseId, publicToken, options) {
    if (typeof databaseId === 'undefined') {
      console.debug('RecombeeClient: Database ID not available.');
    }
    else if (typeof publicToken === 'undefined') {
      console.debug('RecombeeClient: Public token not available.');
    }
    else {
      return new window.recombee.ApiClient(databaseId, publicToken, options);
    }
  }

})(drupalSettings);
