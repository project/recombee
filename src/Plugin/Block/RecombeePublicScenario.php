<?php

namespace Drupal\recombee\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\json_template\Plugin\JsonTemplateManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Recombee Public Scenario' block.
 *
 * @Block(
 *   id = "recombee_public_scenario",
 *   admin_label = @Translation("Recombee Public Scenario"),
 *   category = @Translation("Search")
 * )
 */
class RecombeePublicScenario extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The JSON template manager.
   *
   * @var \Drupal\json_template\Plugin\JsonTemplateManagerInterface
   */
  protected $jsonTemplate;

  /**
   * Constructs a new RecombeePublicScenario instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\json_template\Plugin\JsonTemplateManagerInterface $json_template
   *   The JSON template manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, JsonTemplateManagerInterface $json_template) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->jsonTemplate = $json_template;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.json_template.template')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'endpoint' => '',
      'scenario' => '',
      'template' => '',
      'page_size' => 5,
      'tracking' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->configuration;

    // Get all defined templates.
    $template_options = [];
    foreach ($this->jsonTemplate->getDefinitionsForId('recombee') as $template) {
      $template_options[$template['id']] = $template['title'];
    }

    // Prepare description links.
    $recombee_link = Link::fromTextAndUrl('Recombee', Url::fromUri('https://admin.recombee.com/'))
      ->toString();

    $form['endpoint'] = [
      '#type' => 'select',
      '#title' => $this->t('Recommendation type'),
      '#description' => $this->t('The @recombee Scenario recommendation type.', [
        '@recombee' => $recombee_link,
      ]),
      '#options' => [
        'items-to-item' => $this->t('Items to Item'),
        'items-to-user' => $this->t('Items to User'),
      ],
      '#default_value' => $config['endpoint'],
      '#required' => TRUE,
    ];
    $form['scenario'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scenario'),
      '#description' => $this->t('The @recombee Scenario to use when returning results (see "Scenarios" page for a database).', [
        '@recombee' => $recombee_link,
      ]),
      '#default_value' => $config['scenario'],
      '#required' => TRUE,
    ];
    $form['template'] = [
      '#type' => 'select',
      '#title' => $this->t('Template'),
      '#description' => $this->t('The template to use to display the results.'),
      '#options' => $template_options,
      '#default_value' => $config['template'],
      '#required' => TRUE,
    ];
    $form['page_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Page size'),
      '#description' => $this->t('The number of results to return.'),
      '#default_value' => $config['page_size'],
      '#required' => TRUE,
      '#min' => 1,
    ];
    $form['tracking'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Insert tracking information for all links'),
      '#description' => t('If checked, recommendation block links will receive a tracking ID.'),
      '#default_value' => $config['tracking'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['endpoint'] = $form_state->getValue('endpoint');
    $this->configuration['scenario'] = trim($form_state->getValue('scenario'));
    $this->configuration['template'] = $form_state->getValue('template');
    $this->configuration['page_size'] = intval($form_state->getValue('page_size'));
    $this->configuration['tracking'] = boolval($form_state->getValue('tracking'));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (!$this->jsonTemplate->hasDefinition($this->configuration['template'])) {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('Template "@id" was not found.', [
          '@id' => $this->configuration['template'],
        ]),
      ];
    }
    $build = [
      '#theme' => 'recombee_public_scenario',
      '#endpoint' => $this->configuration['endpoint'],
      '#scenario' => $this->configuration['scenario'],
      '#template' => $this->configuration['template'],
      '#page_size' => $this->configuration['page_size'],
      '#tracking' => $this->configuration['tracking'],
    ];
    /** @var \Drupal\json_template\Plugin\JsonTemplateInterface $plugin */
    $plugin = $this->jsonTemplate->createInstance($this->configuration['template']);
    $plugin->attach($build);
    return $build;
  }

}
