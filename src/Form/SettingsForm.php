<?php

namespace Drupal\recombee\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides configuration form for Recombee module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'recombee_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'recombee.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('recombee.settings');

    // Prepare description links.
    $recombee_link = Link::fromTextAndUrl('Recombee', Url::fromUri('https://admin.recombee.com/'))
      ->toString();

    $form['site_id'] = [
      '#type' => 'textfield',
      '#title' => t('Site ID'),
      '#description' => t('The unique site ID (used for item ID generation).'),
      '#default_value' => $config->get('site_id'),
    ];
    $form['base_url'] = [
      '#type' => 'textfield',
      '#title' => t('Base URL'),
      '#description' => t('The absolute URL to be prefixed to URL fields. E.g. "@url"', [
        '@url' => $GLOBALS['base_url'],
      ]),
      '#default_value' => $config->get('base_url'),
    ];
    $form['allowed_domains'] = [
      '#type' => 'textfield',
      '#title' => t('Allowed Domains'),
      '#description' => t('Specify the domains where the Recombee script is permitted to track. Use a comma to separate multiple values. E.g. "@url"', [
        '@url' => $GLOBALS['base_url'],
      ]),
      '#default_value' => implode(',', $config->get('allowed_domains')) ?? [],
    ];

    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('Public API credentials'),
      '#open' => empty($config->get('account_id')) || empty($config->get('public_token')),
    ];
    $form['api']['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database ID'),
      '#description' => $this->t('The @recombee "API Identifier" value (see "Settings" page for a database).', [
        '@recombee' => $recombee_link,
      ]),
      '#default_value' => $config->get('account_id'),
      '#required' => TRUE,
    ];
    $form['api']['public_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public token'),
      '#description' => $this->t('The @recombee "Public token" value (see "Settings" page for a database).', [
        '@recombee' => $recombee_link,
      ]),
      '#default_value' => $config->get('public_token'),
      '#required' => TRUE,
    ];
    $form['api']['region'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Region'),
      '#description' => $this->t('The @recombee "Region" value (see "Settings" page for a database).', [
        '@recombee' => $recombee_link,
      ]),
      '#default_value' => $config->get('region'),
      '#required' => TRUE,
    ];
    $form['client'] = [
      '#type' => 'details',
      '#title' => $this->t('Recombee client options'),
    ];
    $form['client']['base_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URI'),
      '#description' => t('The base URI for the client connection. If empty, the default is used.'),
      '#default_value' => $config->get('client.base_uri'),
    ];
    $form['client']['use_https'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use HTTPS protocol'),
      '#description' => t('The protocol for the client connection. If unchecked, HTTP is used.'),
      '#default_value' => $config->get('client.use_https'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Check base URL if configured.
    $base_url = $form_state->getValue('base_url');
    if (!empty($base_url)) {
      if (!UrlHelper::isValid($base_url, TRUE)) {
        $form_state->setErrorByName('base_url', t('The base URL is invalid.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('recombee.settings');
    $config->set('site_id', trim($form_state->getValue('site_id')));
    $config->set('base_url', rtrim(trim($form_state->getValue('base_url')), '/'));
    $config->set('allowed_domains', explode(',', trim($form_state->getValue('allowed_domains'))) ?? []);
    $config->set('account_id', trim($form_state->getValue('account_id')));
    $config->set('public_token', trim($form_state->getValue('public_token')));
    $config->set('region', trim($form_state->getValue('region')));
    $config->set('client.base_uri', trim($form_state->getValue('base_uri')));
    $config->set('client.use_https', boolval($form_state->getValue('use_https')));
    $config->save();
  }

}
