# Recombee

## INTRODUCTION

The Recombee module provides blocks for tracking user page views and providing
recommendations to users.

## REQUIREMENTS

The JSON Template module is a dependency and should also be installed.

We recommend installing the Search API Recombee module as it will populate the
Recombee index, providing field data to transform when displaying the results.
If you do not install and configure this module, you will only be getting back
Item IDs, with no further metadata about the item.

Set up a Recombee subscription: https://account.recombee.com/users/sign_up

How much does it cost? https://www.recombee.com/pricing.html

## INSTALLATION

1. The module can be installed via the
[standard Drupal installation process](http://drupal.org/node/1897420).

## CONFIGURATION

To show recommendation results, create a block called "Recombee Public
Scenario". You need to define an existing combination of scenario and its type.
Optionally, follow the instructions of JSON Template module to define your
custom templates.

To track user page views, create a block called "Recombee Tracker". You can
define any kind of conditions, where the block will be displayed (e.g. a content
type or user roles). It doesn't matter where the block is placed, as it is
invisible by default - instead, a tracking code is inserted.

### API documentation

If you are interested you can read more about the Recombee API here:
https://docs.recombee.com/

This will be helpful if you need to trigger your own interaction:
https://docs.recombee.com/api.html#user-item-interactions

### Sample snippets: Item ID and User ID

#### Custom Item ID

It is possible to implement your own functions to return the item ID. The module
does ship with its own default implementation based on a metatag which is set by
the module. This identifier is the same as used by the Search API Recombee
module and so the identity should be aligned. This should work for the majority
of people. However, if you have custom needs you can do it as follows.

```
(function ($) {
  // Attach Item ID callback for Recombee Public Scenario.
  window.RecombeeItemId = function () {
    // Process your logic to fill the item ID.
    window.RecombeeItemId = ... ;
    // Inform main script about success.
    $document.trigger('recombeePropertiesUser');
  }
})(jQuery);
```

#### Custom User ID

It is possible to implement your own function to return the user ID. The
module does ship with its own default implementation based on a random
identifier stored in a first-party cookie. This should work for the majority of
people. However, if you have custom needs you can do it as follows.

```
(function ($) {
  // Attach User ID callback for Recombee Public Scenario.
  window.RecombeeUserId = function () {
    // Process your logic to fill the user ID.
    window.RecombeeUserId = ... ;
    // Inform main script about success.
    window.recombeePropertiesReady = true;
    $(document).trigger('recombeePropertiesReady');
  }
})(jQuery);
```

#### Calling the Recombee client direct

The need may arise to log your own custom events into Recombee. For example, a
product purchase may trigger an event. If your module needs to do this, the
Recombee client is stored in a reliable place for you to use.

```
function YOUR_MODULE_preprocess_SOME_HOOK(&$variables) {
  // Attach Recombee API Client.
  recombee_attach_client($variables);
}
```

```
// Get Recombee Item ID and User ID.
var userId = window.RecombeeUserId;
var itemId = window.RecombeeItemId;
// Define your interaction.
var interaction = new window.recombee.AddPurchase(userId, itemId);
// Send your interaction and catch eventual error.
window.RecombeeClient.send(interaction)
  .catch(function (error) {
    console.error('Recombee AddPurchase: ' + error);
  });
```

### Troubleshooting

#### Scenarios

The recommendations block needs to be configured with a Scenario. A Scenario is
configured in the Recombee backend and is a collection of options that combine
to define a complete query, except for the number of items returned. Setting up
Scenarios is a vital preliminary step before this module can be used.

The machine name of the Scenario needs to be copied over into the block. This
introduces the possibility of human error and care would be taken to enter the
right value.

#### Transforming the results

Non-trivial implementation of the Recombee module will involve the themer
authoring a (Handlebars) template which will be used to convert the JSON to
HTML. Please read the JSON Template documentation for details. Themers will most
likely need to create a small sandbox to refine their transformation.

You can use the following template to log the results into the console.

```
{{log  this}}
```

https://handlebarsjs.com/guide/builtin-helpers.html#each
https://handlebarsjs.com/guide/builtin-helpers.html#log

Doing this will dump the results to the console, giving you the raw results to
work with. You can then use something like the following to iterate on your
template.
http://tryhandlebarsjs.com/

#### Theming

There is Recombee Public Scenario block template you can override in your theme:
`recombee-public-scenario.html.twig`

## MAINTAINERS

These modules are maintained by developers at Morpht. For more information on
the company and our offerings, see [morpht.com](http://morpht.com).
