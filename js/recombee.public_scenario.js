/**
 * @file
 * Initializes all Recombee Public Scenario blocks.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  if (typeof window.recombeePropertiesReady === 'undefined') {
    $(document).on('recombeePropertiesReady', init);
  }
  else {
    init();
  }

  function init() {
    Drupal.behaviors.recombeePublicScenario = {
      attach: function attach(context, settings) {

        if (typeof settings.recombeePublicScenario === 'undefined') {
          return;
        }

        $.each(settings.recombeePublicScenario, function (id, args) {
          var $element = $('#' + id + ':not(.processed)', context);
          if ($element.length) {
            getRecommendations($element, args.template, args);
            $element.addClass('processed');
          }
        });

      }
    };
    Drupal.behaviors.recombeePublicScenario.attach(document, drupalSettings);
  }

  function getRecommendations(block, template, args) {
    var options = {
      scenario: args.scenario,
      returnProperties: true,
    };
    var itemId = window.RecombeeItemId;
    var userId = window.RecombeeUserId;
    var interaction = null;

    switch (args.endpoint) {
      case 'items-to-item':
        if (typeof itemId !== 'undefined') {
          interaction = new window.recombee.RecommendItemsToItem(itemId, userId, args.pageSize, options);
        }
        else {
          console.error('Recombee: Item ID not available.');
        }
        break;

      case 'items-to-user':
        if (typeof userId !== 'undefined') {
          interaction = new window.recombee.RecommendItemsToUser(userId, args.pageSize, options);
        }
        else {
          console.error('Recombee: User ID not available.');
        }
        break;

      default:
        console.error('Recombee: Interaction "' + args.endpoint + '" not available.');
        break;
    }
    if (interaction) {
      window.RecombeeClient.send(interaction)
        .then(renderRecommendations(block, template, args.tracking))
        .catch(function (error) {
          console.error('Recombee: ' + error);
        });
    }
  }

  function renderRecommendations(block, template, tracking) {
    return function (result) {
      block.html(Drupal.jsonTemplate.render(result, template));
      if (tracking) {
        insertTracking(block, result.recommId);
      }
    }
  }

  function insertTracking(block, recommId) {
    $('a', block).each(function () {
      var url = new URL(this.href);
      if (url.search.length > 0) {
        url.search += '&recombee-recommid=' + recommId;
      }
      else {
        url.search = '?recombee-recommid=' + recommId;
      }
      this.href = url;
    });
  }

})(jQuery, Drupal, drupalSettings);
