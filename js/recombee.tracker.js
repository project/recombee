/**
 * @file
 * Initializes Recombee Tracker.
 */

(function ($, settings) {

  'use strict';

  if (typeof window.recombeePropertiesReady === 'undefined') {
    $(document).on('recombeePropertiesReady', init);
  }
  else {
    init();
  }

  function init() {
    var config = settings.recombeeClient;
    var allowedDomains = config.allowed_domains;
    if (!allowedDomains.length || !allowedDomains.includes(window.location.origin)) {
      return;
    }
    var userId = window.RecombeeUserId;
    var itemId = window.RecombeeItemId;
    if (typeof userId === 'undefined') {
      console.debug('RecombeeTracker: User ID not available.');
    }
    else if (typeof itemId === 'undefined') {
      console.debug('RecombeeTracker: Item ID not available.');
    }
    else {
      var options = {};
      var recommId = getQueryParam('recombee-recommid');
      if (typeof recommId !== 'undefined') {
        options.recommId = recommId;
      }
      var interaction = new window.recombee.AddDetailView(userId, itemId, options);
      window.RecombeeClient.send(interaction)
        .catch(function (error) {
          console.error('Recombee: ' + error);
        });
    }
  }

  function getQueryParam(key) {
    var params = window.location.search.substring(1).split('&');
    for (var i = 0; i < params.length; i++) {
      var param = params[i].split('=');
      if (param[0] === key) {
        return param[1] === undefined ? true : decodeURIComponent(param[1]);
      }
    }
  }

})(jQuery, drupalSettings);
