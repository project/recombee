/**
 * @file
 * Initializes Recombee Properties.
 */

(function ($, cookies) {

  'use strict';

  var $document = $(document);
  $document.on('recombeePropertiesUser', getUserId);
  getItemId();

  function getItemId() {
    if (typeof window.RecombeeItemId === 'function') {
      return window.RecombeeItemId();
    }
    return getDefaultItemId();
  }

  function getDefaultItemId() {
    var $itemIdMeta = $('meta[name="recombee-item-id"]');
    if ($itemIdMeta.length) {
      window.RecombeeItemId = $itemIdMeta.attr('content');
    }
    else {
      console.debug('RecombeeProperties: Meta item ID not available.');
    }
    $document.trigger('recombeePropertiesUser');
  }

  function getUserId() {
    if (typeof window.RecombeeUserId === 'function') {
      return window.RecombeeUserId();
    }
    return getDefaultUserId();
  }

  function getCookieConsent() {
    const globalCookieConsent = 1;
    var cookieConsent = cookies.get('RecombeeCookieConsent');
    if (globalCookieConsent !== 'undefined' && !cookieConsent) {
      // Default Global defined cookieconsent.
      cookieConsent = globalCookieConsent;
    }

    cookies.set('RecombeeCookieConsent', cookieConsent, {expires: 365, path: '/'});
    return cookieConsent;
  }

  function getDefaultUserId() {
    var value = cookies.get('RecombeeUserId');
    if (typeof value === 'undefined') {
      var arr = new Uint8Array(10);
      (window.crypto || window.msCrypto).getRandomValues(arr);
      value = '';
      for (var i = 0; i < arr.length; i++) {
        value += ('0' + arr[i].toString(16)).substr(-2);
      }
      var cookieConsent = getCookieConsent();
      // Respect the cookieConsent preference of the user.
      if (cookieConsent == 1) {
        cookies.set('RecombeeUserId', value, {expires: 365, path: '/'});
      }
    }
    window.RecombeeUserId = value;
    window.recombeePropertiesReady = true;
    $document.trigger('recombeePropertiesReady');
  }

})(jQuery, window.Cookies);
