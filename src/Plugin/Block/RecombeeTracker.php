<?php

namespace Drupal\recombee\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Recombee Tracker' block.
 *
 * @Block(
 *   id = "recombee_tracker",
 *   admin_label = @Translation("Recombee Tracker"),
 *   category = @Translation("Search")
 * )
 */
class RecombeeTracker extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#attached' => ['library' => ['recombee/tracker']],
      '#attributes' => ['class' => ['hidden']],
    ];
    recombee_attach_client($build);
    return $build;
  }

}
